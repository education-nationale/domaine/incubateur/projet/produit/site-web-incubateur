
const fs = require('fs');
module.exports = function (eleventyConfig) {
	  // Input directory: src
	  // Output directory: _site
    // Copy the contents of the `public` folder to the output folder
    // For example, `./src/css/` ends up in `_site/css/`
    eleventyConfig.addPassthroughCopy({
        "./node_modules/@gouvfr/dsfr/dist/favicon": "/favicon",
        "./node_modules/@gouvfr/dsfr/dist/fonts": "/css/fonts",
        "./node_modules/@gouvfr/dsfr/dist/icons": "/css/icons",
        "./node_modules/@gouvfr/dsfr/dist/dsfr.min.css": "/css/dsfr.min.css",
        "./node_modules/@gouvfr/dsfr/dist/utility/utility.min.css": "/css/utility/utility.min.css",
        "./node_modules/remixicon/":"/css/remixicon",
        "./node_modules/@gouvfr/dsfr/dist/dsfr.module.min.js": "/js/dsfr.module.min.js",
        "./node_modules/@gouvfr/dsfr/dist/dsfr.nomodule.min.js": "/js/dsfr.nomodule.min.js",
        "./node_modules/@gouvfr/dsfr/dist/artwork": "/images/artwork",
        "./src/assets/css/styles.css": "/css/styles.css",
        "./src/assets/js/search.js": "/js/search.js",
        "./src/assets/images": "/images",
        "./node_modules/mermaid/dist/mermaid.min.js":"/js/mermaid.min.js"
    });

    // Transformation pour repérer les blocs de code Mermaid et les convertir
    eleventyConfig.addTransform("mermaid", function(content) {
      if (this.outputPath && this.outputPath.endsWith(".html")) {
        // Remplacer les blocs de code mermaid par des divs mermaid
        return content.replace(/<pre><code class="language-mermaid">([\s\S]*?)<\/code><\/pre>/g, function(_, code) {
          return `<div class="mermaid">${code.trim()}</div>`;
        });
      }
      return content;
    });

    //Personnalisation du rendu markdown
    require('./dsfr-custom-markdown')(eleventyConfig);

    // Ajouter le filtre 'split' à Eleventy
    eleventyConfig.addFilter("split", function(str, separator) {
      return str.split(separator);
    });

    // Ajouter un filtre personnalisé appelé "sortByDate" à la configuration d'Eleventy
    eleventyConfig.addFilter("sortByDate", function(values) { 
      // Créer une copie du tableau "values" et le trier par date
      // "values.slice()" est utilisé pour créer une copie peu profonde du tableau
      return values.slice().sort((a, b) => {
        
        // Convertir les dates en objets Date et les comparer
        // La différence entre les dates permet de trier le tableau
        // Ici, "b.date - a.date" trie en ordre décroissant (du plus récent au plus ancien)
        return new Date(b.date) - new Date(a.date);
      });
    });

    // Ajouter un filtre personnalisé appelé "sortByTitle" à la configuration d'Eleventy
    eleventyConfig.addFilter("sortByTitle", function(values) {
      // Créer une copie du tableau "values" et le trier par titre
      // "values.slice()" est utilisé pour créer une copie peu profonde du tableau
      return values.slice().sort((a, b) => {
        
        // Convertir les titres en chaînes de caractères et les comparer
        // La méthode localeCompare() compare deux chaînes en tenant compte de la locale
        return a.data.title.localeCompare(b.data.title);
      });
    });

    // Filtre personnalisé pour supprimer les balises HTML
    eleventyConfig.addFilter("stripHtml", function(content) {
      return content.replace(/<\/?[^>]+(>|$)/g, ""); // Supprime toutes les balises HTML
    });


    // Ajoute une collection personnalisée appelée "mainPages" dans Eleventy
    eleventyConfig.addCollection("mainPages", function(collectionApi) {
      // Utilise l'API de collection d'Eleventy pour obtenir toutes les pages et les filtrer
      return collectionApi.getAll().filter(function(item) {
        // Récupère les tags associés à l'élément, ou un tableau vide si aucun tag n'est défini
        const tags = item.data.tags || [];
        // Récupère le titre de l'élément
        const title = item.data.title;
        // Vérifie si les tags ou le titre contiennent des valeurs à exclure
        // Exclut les éléments qui ont le tag 'product' ou 'news'
        // ou dont le titre contient 'taglist'
        const hasExcludedTag = tags.includes("product") || tags.includes("news") || title.includes('taglist');
        // Retourne false si l'élément doit être exclu, true sinon
        return !hasExcludedTag;
      });
    });
    // Ajoute une collection personnalisée de recherche de contenu dans tout le site
    eleventyConfig.addCollection('searchItems',function (collectionApi) {
      return collectionApi.getAll().filter(function(item) {
        // Récupère les tags associés à l'élément, ou un tableau vide si aucun tag n'est défini
        const tags = item.data.tags || [];
        // Récupère le titre de l'élément
        const title = item.data.title;
        // Vérifie si le titre contient 'taglist'
        const hasExcludedTag = title.includes('taglist');
        // Retourne false si l'élément doit être exclu, true sinon
        return !hasExcludedTag;
      });
    });

    eleventyConfig.addCollection("tagList", function(collectionApi) {
      // Crée un nouvel ensemble (Set) pour stocker des tags uniques
      let tagSet = new Set();
      // https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Set
      // Un Set est une structure de données qui permet de stocker des valeurs uniques. Ici, il est utilisé pour garantir que chaque tag n'est ajouté qu'une seule fois, même s'il apparaît dans plusieurs articles.
      // Parcourt tous les éléments collectés par Eleventy
      collectionApi.getAll().forEach(function(item) {
        // Vérifie si l'élément a une propriété 'tags' dans ses données
        if ("tags" in item.data) {
          let tags = item.data.tags;
    
          // Optionnel: filtre pour exclure certains tags spécifiques
          // Dans cet exemple, le tag 'product' est exclu
          tags = tags.filter(tag => tag !== "product");
          tags = tags.filter(tag => tag !== "news");
          tags = tags.filter(tag => tag !== "team");
        
          // Parcourt les tags restants et les ajoute à l'ensemble
          for (const tag of tags) {
            tagSet.add(tag);
          }
        }
      });
      // Retourne un tableau contenant tous les tags uniques (conversion du Set en tableau)
      return [...tagSet];
    });

    return {
        dir: {
          input: "src",       // Répertoire source
          includes: "_includes", // Répertoire des layouts et partials
          data: "_data",      // Répertoire des fichiers de données
          output: "_site"     // Répertoire de sortie
        },
        markdownTemplateEngine: "njk", // Utilisation de Nunjucks pour le rendu des templates Markdown
    };

	
};