module.exports = {
    
    // Titre du site ou du service
    title: "Incubateur de produits numériques",

    // URL principale du site
    url: "http://localhost:8081",

    // URL beta gouv
    beta_gouv_url: "https://beta.gouv.fr/",

    // Informations sur la newsletter (à compléter)
    newsletter: [
        {
            url: "", // URL de la newsletter
            title: "", // Titre de la newsletter
            description: "" // Description de la newsletter
        }
    ],

    // URLs des réseaux sociaux (à compléter)
    facebook_url: "https://www.facebook.com/sharer.php?u=",
    mastodon_url: "",
    twitter_url: "https://twitter.com/intent/tweet?url=",
    instagram_url: "",
    linkedin_url: "https://www.linkedin.com/shareArticle?url=",
    youtube_url: "",
    peertube_url: "",
    github_url: "",
    sourcehut_url: "",

    // URL du dépôt de code source du projet
    repository_url: "",

    // Langue principale du site
    language: "fr",

    // Description du service
    description: "L'innovation pour l'éducation",

    // Informations sur l'auteur
    author: {
        email: "contact@nom.du.service.gouv.fr",
        url: "https://url_du_service.gouv.fr/"
    },

    // liens internes
    method_url: "methode",
    team_url: "notre-equipe",
    products_url: "services-numeriques",
    ressources_url: "ressources",

    // Nom de l'entité responsable du service
    service: "Incubateur de produits numériques (Ministère de l'Éducation nationale et de la Jeunesse)"
};
