// _data/team.js

const {fakerFR } = require('@faker-js/faker');


module.exports = () => {
  const members = [];

  for (let i = 0; i < 6; i++) {
    members.push({
      name: fakerFR.person.fullName(),
      role: fakerFR.person.jobTitle(),
      bio: fakerFR.person.bio(),
      photo: fakerFR.image.url(),
      social: {
        linkedin: fakerFR.internet.url(),
        github: fakerFR.internet.url(),
      }
    });
  }

  return { members };
};
