// Fonction asynchrone pour récupérer le fichier JSON contenant l'index de recherche
async function fetchSearchIndex() {
    // Utilisation de fetch pour récupérer le fichier JSON depuis l'URL /search-index.json
    const response = await fetch('/api/search-index.json');
    
    // Conversion de la réponse en JSON et retour du résultat
    return await response.json();
  }
  
  // Fonction qui effectue la recherche dans l'index en fonction de la requête utilisateur
  function search(query, index) {
    // Filtrage de l'index en vérifiant si le titre ou le contenu de la page contient la requête
    return index.filter(page => {
      return page.title.toLowerCase().includes(query.toLowerCase()) ||
             page.content.toLowerCase().includes(query.toLowerCase());
    });
  }
  
  function displayResults(results) {
    // Sélection du conteneur où les résultats seront affichés
    const resultsContainer = document.querySelector('#search-results');
    
    // Réinitialisation du contenu du conteneur avant d'afficher les nouveaux résultats
    resultsContainer.innerHTML = '';
  
    // Si aucun résultat n'est trouvé, afficher un message indiquant "Aucun résultat trouvé."
    if (results.length === 0) {
      resultsContainer.innerHTML = '<div class="fr-container fr-my-5w"><p>Aucun résultat trouvé.</p></div>';
      return;
    }
  
    // Création de la div contenant les résultats sous forme de grid
    const resultsWrapper = document.createElement('div');
    resultsWrapper.className = 'fr-grid-row fr-mb-3w'; // Utilisation des classes de la grille
  
    // Parcourir les résultats et les afficher dynamiquement dans des cartes
    results.forEach(result => {
      // Création d'un élément div pour chaque carte
      const cardItem = document.createElement('div');
      cardItem.className = 'fr-col fr-col-md-6'; // Chaque carte occupe une colonne de taille md-6
  
      // Déséchapper les guillemets dans le contenu avant de l'afficher
      const sanitizedContent = result.content.replace(/\\"/g, '"').replace(/\\n/g, '<br>'); // Remplace \" par " et \n par <br>
  
      // Formatage du HTML pour chaque carte (titre, description et image par défaut)
      cardItem.innerHTML = `
        <div class="fr-card fr-enlarge-link fr-m-2v">
          <div class="fr-card__body">
            <div class="fr-card__content">
              <h3 class="fr-card__title">
                <a href="${result.url}">${result.title}</a>
              </h3>
              <p class="fr-card__desc">${sanitizedContent.substring(0, 150)}...</p>
            </div>
          </div>
          <div class="fr-card__header">
            
          </div>
        </div>
      `;
  
      // Ajouter chaque carte à la grid des résultats
      resultsWrapper.appendChild(cardItem);
    });
  
    // Ajout du wrapper avec les résultats sous forme de grid dans le conteneur principal
    resultsContainer.appendChild(resultsWrapper);
  }
  
  
  // Ajout d'un écouteur d'événement sur le bouton de recherche pour déclencher la recherche par clic
  document.querySelector('#search-button').addEventListener('click', async () => {
    // Récupération de la valeur du champ de saisie
    const query = document.querySelector('#search-557-input').value;
  
    // Si la requête contient plus de 2 caractères, commencer la recherche
    if (query.length > 2) { 
      // Récupérer l'index de recherche à partir du fichier JSON
      const index = await fetchSearchIndex();
      
      // Effectuer la recherche avec la requête utilisateur et l'index récupéré
      const results = search(query, index);
      
      // Afficher les résultats dans le conteneur
      displayResults(results);
    }
  });
  