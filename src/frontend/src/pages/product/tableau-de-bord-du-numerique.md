---
title: Tableau de bord de la stratégie
tags: ["DNE", "produit"]
description: "Suivez la mise en œuvre de la stratégie du numérique pour l'éducation."
# statuts posssibles : investigation, construction, accélération et transfert
status: "construction"
# secteurs possibles : startup, produit ou veille
sector: "produit"
image: "strategie"
link: https://icr-dev-app00017.incubateur.in.cloe.education.gouv.fr:8080/
repo: 
date: 2024-08-29
---

### 4 axes, 16 actions clés et 36 objectifs.

 Cet outil vous permet d’évaluer l'avancement des actions de la stratégie et les orientations qui sont prises pour atteindre les différents objectifs. Le ministère met à jour tous les trois mois les informations du tableau de bord.

### Tester le produit

```sh
git clone https://gitlab.forge.education.gouv.fr/men/domaine/incubateur/tableau-de-bord-de-la-strategie.git
```