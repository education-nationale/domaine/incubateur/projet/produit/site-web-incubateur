---
title: La première version du portefeuille
tags: ["IA","DNE"]
description: 15 produits
image: "lorem"
date: 2024-08-19
---



### Tableau d'avancement des produits incubés

| Phase         | Filière Startup d’État                                                                 | Filière Produit           | Filière Veille         |
| ------------- | -------------------------------------------------------------------------------------- | ------------------------- | ---------------------- |
| **Investigation** | - Pix persévérance (DGESCO)<br/> - Suivi de la progression des élèves (PE 1D) <br/> - 1 jeune 1 arbre (MASA + DGESCO) | - Bâti scolaire (SG) <br/> - APN <br/> - Partage de données par Datapass | - IAgen : Edu-GPT (DNE+IGESR) <br/> - Comparatifs outils data (DNE) <br/> - Trace d’apprentissage (DNE) <br/> - Conso énergétique (DNE)  |
| **Construction**  | - Compte ressources  | - Edupilote <br/> - Evaluation des écoles (CEE) |  |
| **Accélération**  | - Stages scolaires : 3e et 2de (DGESCO)  |  - [Tableau de bord de la stratégie] |  |
| **Transfert**     |  |  |  |


[Tableau de bord de la stratégie]: <https://icr-dev-app00017.incubateur.in.cloe.education.gouv.fr:8080/>