---
title: Zoom sur l'IA & l'IA générative
tags: ["IA","DNE"]
description: "Installation d’un prompt interne"
image: ""
date: 2024-08-29
---


## Premières actions

- **Installation d’un prompt interne** et d’un modèle [LLAMA3] pour tester les apports de l’IA générative, sans transmettre des données à l’extérieur du ministère Quelques comptes ont été créés à des fins de tests.
- **Prêt à la demande** de quelques licences ChatGPT
- **Mise en place d’une plateforme pour cartographier les initiatives IA** et IA générative sur le territoire.
- **Des comptes ont été créés pour les premières régions académiques** (en lien avec le tour de France de la DNE) et quelques directions métiers


[LLAMA3]: <https://alliance.numerique.gouv.fr/>
