// dsfr-custom-markdown.js

module.exports = function(eleventyConfig) {
    // Importer la bibliothèque markdown-it, utilisée pour parser le Markdown en HTML
    const markdownIt = require("markdown-it");
  
    // Créer une instance de markdown-it avec des options personnalisées
    const md = markdownIt({
      html: true, // Autoriser le HTML brut dans le Markdown
      breaks: true, // Convertir les retours à la ligne simples en <br>
      linkify: true // Convertir automatiquement les URLs en liens cliquables
    });
  
    // Sauvegarder la règle de rendu par défaut pour les balises <table>
    const defaultRender = md.renderer.rules.table_open || function(tokens, idx, options, env, self) {
      return self.renderToken(tokens, idx, options);
    };
  
    // Redéfinir la règle de rendu pour les balises <table> pour ajouter une structure spécifique
    md.renderer.rules.table_open = function(tokens, idx, options, env, self) {
      // Ajouter la classe 'fr-table' à la balise <table>
      tokens[idx].attrPush(['class', 'fr-table fr-table--lg']); // Ajouter les classes nécessaires à la balise <table>
      
      // Débuter la structure de divs avant la balise <table>
      let wrapperStart = `<div class="fr-container">` +
                         `<div class="fr-table--md fr-table" id="table-lg-component">` +
                         `<div class="fr-table__container">` +
                         `<div class="fr-table__content">`;
      
      // Continuer le rendu par défaut de la balise <table>
      return wrapperStart + defaultRender(tokens, idx, options, env, self);
    };
  
    // Redéfinir la règle de rendu pour la fermeture des balises <table>
    md.renderer.rules.table_close = function(tokens, idx, options, env, self) {
      // Fermer la structure de divs après la balise </table>
      let wrapperEnd = `</div></div></div></div>`;
      
      // Continuer le rendu par défaut de la balise </table>
      return defaultRender(tokens, idx, options, env, self) + wrapperEnd;
    };
  
    // Dire à Eleventy d'utiliser cette instance personnalisée de markdown-it pour traiter les fichiers Markdown
    eleventyConfig.setLibrary("md", md);
  };
  